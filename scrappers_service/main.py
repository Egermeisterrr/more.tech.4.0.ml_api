from scrup.scrappers import *
import os
import time

files = ['scrap_consultant.py', 'scrap_kommersant.py', 'scrap_nalog-nalog.py', 'scrap_v2b.py', 'scrap_buh.py',
         'scrap_gb.py', 'scrap_mk.py', 'scrap_ria.py', 'why42.py']


abs_path = "./scrup"

while True:
    with open(abs_path + "/data.json", "a+", encoding="utf-8") as f:
        f.seek(0, 2)
        size = f.tell()
        f.truncate(size - 2)
        f.write(",\n")

    for file in files:
        os.system(f'python scrup/scrappers/{file}')
        time.sleep(3)

    with open(abs_path + "/data.json", "a+", encoding="utf-8") as f:
        f.seek(0, 2)
        size = f.tell()
        f.truncate(size - 3)
        f.write("}]")

    time.sleep(60 * 20)
