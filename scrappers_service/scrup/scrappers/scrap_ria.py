import requests
from bs4 import BeautifulSoup
import json
import pandas as pd


def get_data(url):
    headers = {}

    req = requests.get(url, headers)
    soup = BeautifulSoup(req.text, "lxml")

    txt = ''

    for i in soup.find_all('div', class_='article__text'):
        txt += i.text

    return txt


def get_time(url):
    headers = {}

    req = requests.get(url, headers)
    soup = BeautifulSoup(req.text, "lxml")

    txt = ''

    for i in soup.find_all('div', class_='article__info-date'):
        txt += i.text

    return txt


def sphereBeautyMaker(soap):
    # используем парсер lxml
    # soup = BeautifulSoup(html_text, 'lxml')
    soup = soap
    mapq = {}
    id = 0
    # получаем все элементы
    ads = soup.find_all('div', class_='list-item')
    for i in (range(len(ads))):

        ad = ads[i]
        id += 1
        mapq[id] = {}
        tags = []

        content = ad.find('div', class_='list-item__content')
        imag = content.find('img', class_="responsive_img m-list-img")

        mapq[id]["title"] = content.text
        mapq[id]["href"] = str(content)[str(content).find("href=") + 6: str(content).find("<picture>") - 2]
        mapq[id]["text"] = str(
            get_data(str(content)[str(content).find("href=") + 6: str(content).find("<picture>") - 2]))
        mapq[id]["text"] = mapq[id]["text"][mapq[id]["text"].find(".") + 2:]
        mapq[id]["time"] = str(
            get_time(str(content)[str(content).find("href=") + 6: str(content).find("<picture>") - 2]))
        mapq[id]["img"] = str(imag)[str(imag).find("data-responsive0=") + 18:str(imag).find("data-responsive375") - 2]

        tag = ad.find('div', class_='list-item__tags')

        for i in tag.find_all('a'):
            tags.append(i.text)

        mapq[id]["tags"] = tags
    result = []
    cur_row = 0
    for id in mapq.keys():
        dct = {"title": mapq[id]["title"], "tags": mapq[id]["tags"], "time": mapq[id]["time"], "text": mapq[id]["text"],
               "img": mapq[id]["img"]}
        result.append(dct)

    cur_row += 1
    print(result)
    return result


urls = ['https://ria.ru/politics/', 'https://ria.ru/world/', 'https://ria.ru/economy/', 'https://ria.ru/society/',
        'https://ria.ru/incidents/', 'https://ria.ru/science/', 'https://ria.ru/culture/']

# делаем запрос и получаем html
data = []
for url in urls:
    html_text = requests.get(url).text
    res = (sphereBeautyMaker(BeautifulSoup(html_text, 'lxml')))
    data += res


abs_path = "./scrup"

with open(abs_path + "/title.txt", "a+", encoding="utf-8") as f:
    f.seek(0)
    titles = f.read().split(";;;\n")
    with open(abs_path + "/data.json", "a", encoding="utf-8") as f1:
        for site in data:
            if site["title"] not in titles:
                f.write(site["title"]+";;;\n")
                json.dump(site, f1, sort_keys=False, indent=4, ensure_ascii=False, separators=(',', ': '))
                f1.write(",\n")
