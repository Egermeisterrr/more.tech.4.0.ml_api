# kommersant
import requests
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup, Comment
import json
import sys


def get_categories(url, headers):
    """"""
    req = requests.get(url, headers)
    bs = BeautifulSoup(req.text, headers)

    data = bs.find_all("ul", {"class": "site_menu__list"})
    categorical = data[2].find_all("li", {"class": "site_menu__item"})

    res = []
    for i in categorical:
        temp = i.find("a")
        href = url + temp["href"]
        text = temp.text
        res.append((text, href))

    return res


def get_articles(url, headers, main_url, max_len=0):
    """"""
    req = requests.get(url, headers)
    bs = BeautifulSoup(req.text, headers)

    articles = bs.find_all("h2", {"class": "uho__name"})
    if max_len == 0: max_len = len(articles)

    return [main_url + art.find("a")["href"] for art in articles[:max_len]]


def get_data(url, headers, main_tag):
    """put data from article"""

    req = requests.get(url, headers)
    bs = BeautifulSoup(req.text, headers)

    title_pre = bs.find("h1", {"class": "doc_header__name"})
    tag_pre = bs.find("li", {"class": "crumbs__item"})
    time_pre = bs.find("time", {"class": "doc_header__publish_time"})
    text_pre = bs.find("div", {"class": "doc__body"}).find_all("p")
    img_pre = bs.find_all("img", {"class": "doc_media__media_free"})

    title = title_pre.text.replace("\n", "").replace("\r", "")
    tag = tag_pre.find("a").text
    time = time_pre.text
    text = " ".join([i.text for i in text_pre])
    img = img_pre[0]["data-lazyimage-src"] if len(img_pre) > 1 else ""  # img can not be on the page

    dct = {"title": title, "tags": [main_tag, tag], "time": time, "text": text, "img": img}

    return dct


# url = "https://www.kommersant.ru/doc/5605704"
# data = get_data(url, {}, "")
# print(data)


def scrap(site_url):
    headers = {}
    categories = get_categories(site_url, headers)

    data = []

    for cat_name, cat_url in categories:
        hrefs = get_articles(cat_url, headers, site_url, max_len=3)
        for href in hrefs:
            print(f"{cat_name}")
            site_info = get_data(href, headers, cat_name)
            data.append(site_info)

    print("All is well!")
    return data


url = "https://www.kommersant.ru"
lst1 = []
data = scrap(url)
abs_path = "./scrup"


with open(abs_path + "/title.txt", "a+", encoding="utf-8") as f:
    f.seek(0)
    titles = f.read().split(";;;\n")
    with open(abs_path + "/data.json", "a", encoding="utf-8") as f1:
        for site in data:
            if site["title"] not in titles:
                f.write(site["title"]+";;;\n")
                json.dump(site, f1, sort_keys=False, indent=4, ensure_ascii=False, separators=(',', ': '))
                f1.write(",\n")





