from fastapi import FastAPI
import uvicorn

from serve import euclidean, csv_to_json

app = FastAPI()


@app.get("/")  # нужно будет потом удалить
async def hello_world():
    return {"message": "Hello world!"}


@app.get("/digest")
async def get_digest(sphere: str, sphere_description: str):
    return euclidean(sphere + "\n" + sphere_description)


@app.get("/insights")  # ничего принимать не надо
async def get_insights():
    return {"message": "Hello insights!"}


@app.get("/trends")  # ничего принимать не надо
async def get_trends():
    return {"message": "Hello trends!"}


if __name__ == '__main__':
    uvicorn.run("main:app", port=8000, host="127.0.0.1", reload=True)
